import axios from 'axios'
import VueRouter from 'vue-router'
import routes from './router/routes'

export const http = {
    install(Vue, options) {
        Vue.prototype.$http = Vue.http = axios.create()
    }
}

export {
    router
}