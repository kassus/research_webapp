import VueRouter from 'vue-router'
import Admin from '../Features/User/Admin.vue'
import Login from '../Features/User/Login.vue'
import Graphs from '../Features/Graphs/Graphs.vue'
import UserProfile from '../Features/User/UserProfile.vue'
import ResearchFiles from '../Features/Files/ResearchFiles.vue'
import AddUser from '../Features/User/AddUser.vue'

const routes = [{
        path: '/admin',
        component: Admin
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/graphs',
        component: Graphs
    },
    {
        path: '/researchFiles',
        component: ResearchFiles
    },
    {
        path: '/userProfile',
        component: UserProfile
    },
    {
        path: '/addUser',
        component: AddUser
    },
    {
        path: '/editUser',
        component: AddUser
    },
];

export default routes;