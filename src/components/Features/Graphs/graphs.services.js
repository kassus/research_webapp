import axios from 'axios'
import VueAxios from 'vue-axios'
import routes from '../router/routes'
import apiUrl from '../_router/index'


export default class GraphService {
    constructor() {

    }
    // Get all graphs
    getAllGraphs = () => {
        Vue.axios.get(apiUrl).then((response) => {
                return response.data;
            })
            .catch(() => {

            })
    }

    // This is use for updating graph by its Id
    updateGraph = (graphId) => {
        Vue.axios.get(apiUrl , graphId).then((response) => {
                return response.data;
            })
            .catch(() => {

            })
    }

    // This is use for deleting graph by its Id
    deleteGraph = (graphId) => {
        Vue.axios.get(apiUrl, +graphId).then((response) => {
                return response.data;
            })
            .catch(() => {

            })
    }
}