import axios from 'axios'
import VueAxios from 'vue-axios'
import routes from '../_router/routes'
import apiUrl from '../_router/index'

const apiUrl = "http://localhost:3000/"

export default class FileService {
    constructor() {

    }

    // Get all files
    getFiles = () => {
        Vue.axios.get(apiUrl).then((response) => {
                return response.data;
            })
            .catch(() => {

            })
    }
    // get file
    getFieById = (fileId) => {
        Vue.axios.get(apiUrl, +fileId).then((response) => {
                return response.data;
            })
            .catch(() => {})
    }

    // This is use for deleting file by its Id
    deleteFile = (fileId) => {
        Vue.axios.get(apiUrl , + fileId).then((response) => {
                return response.data;
            })
            .catch(() => {})
    }






}